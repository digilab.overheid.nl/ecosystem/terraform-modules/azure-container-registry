module "container-registry-public" {
  source = "../../"

  resource_group_name = "mygroup"
  location            = "West Europe"
  name                = "myregistry"
}
