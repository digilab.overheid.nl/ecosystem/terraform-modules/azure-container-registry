variable "resource_group_name" {
  description = "The name of the resource group."
  type        = string
}

variable "location" {
  description = "The name of the Azure region."
  type        = string
}

variable "name" {
  description = "The name of the container registry."
  type        = string
}

variable "anonymous_pull_enabled" {
  description = "Allow unauthenticated users to pull from the registry."
  type        = bool
  default     = false
}
