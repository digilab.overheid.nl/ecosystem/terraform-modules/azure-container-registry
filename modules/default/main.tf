resource "azurerm_resource_group" "default" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_container_registry" "default" {
  name                   = var.name
  resource_group_name    = azurerm_resource_group.default.name
  location               = azurerm_resource_group.default.location
  sku                    = "Standard"
  anonymous_pull_enabled = var.anonymous_pull_enabled
}
